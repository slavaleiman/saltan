#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

# стартуют одновременно скрипты эмулятор сименса и эмулятор садко

import os
from subprocess import call
from subprocess import Popen

# subprocess.call(['python', 'exampleScripts.py', somescript_arg1, somescript_val1,...]).

# Popen(['python3', 'sadko_emulator.py'],
#                  stdout=open('null1', 'w'),
#                  stderr=open('logfile.log', 'a'),
#                  start_new_session=True )

Popen(['python3', 'siemens_emulator.py', '/dev/ttyUSB0', '9600'],
                 start_new_session=True )

Popen(['python3', 'sadko_emulator.py', '/dev/ttyUSB0', '115200'],
                 start_new_session=True )
