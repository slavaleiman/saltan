#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate] [count]" % sys.argv[0])
    sys.exit(1)

set_port(sys.argv[1], sys.argv[2])

msgnum = int(sys.argv[3])
if not msgnum:
    msgnum = 1

ADDR = 0xE

def get_msg_size(mb_response):
    # A1  A0  Q1  Q0  N   D (N байт)
    print("response:", mb_response[27:32].replace(" ",""))
    data_available = int(mb_response[27:32].replace(" ",""), 16)
    # print("data_available:", data_available)
    return data_available

def get_first_and_last_id(data):
    # print("data:", data)
    first_id = 0
    last_id = 2
    
    return (first_id, last_id)

out = ""
ret = read_input_reg(ADDR, 0x1000, 6, sleep_time=0.1)
if ret:
    out += "request :" + str(ret[0]) + "\n"
    out += "response:" + str(ret[1]) + "\n"

    if len(ret[1]) < 16:
        exit(0)

    data_items_count = get_msg_size(ret[1])
    out += "item count : " + str(data_items_count) + "\n"

    if(data_items_count):
        input_bytes = read_input_reg(ADDR, 0x2000, data_items_count, 0.1)
        out += "request: " + str(input_bytes[0]) + "\n"
        out += "response :"+ str(input_bytes[1]) + "\n"

        ret = write_multiple_regs(ADDR, 0x1000, (1,1,1,msgnum), sleep_time=0.1)
        if ret:
            if ret[0]:
                out += "approve :"+ str(ret[0]) + "\n"
            if ret[1]:
                out += "approve :"+ str(ret[1]) + "\n"
    # system('clear')
print(out)

time.sleep(0.1)
