#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 3:
    print("USAGE: %s [port] [baudrate] [count]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2], timeout=3.0)

msgnum = int(sys.argv[3])
if not msgnum:
    msgnum = 1
num = 0

prime_numbers = [2, 3, 5, 7]
byte_array = bytearray(prime_numbers)

# output_string1 = [0xA, 0xA, 0xA, 0xA, 0xA]
output_string2 = [0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA]
# output_string3 = [0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA]

for i in range(msgnum):
    if(ser.isOpen() == False):
        ser.open()

    # output_string1[i % len(output_string1)] = i % 255
    output_string2[i % len(output_string2)] = i % 255
    # output_string3[i % len(output_string3)] = i % 255

    num += 1
    # cmd_bytes = bytearray(output_string1)
    # print(str(num) + " -> " + str(cmd_bytes))
    # ser.write(cmd_bytes)
    # time.sleep(0.03)

    cmd_bytes = bytearray(output_string2)
    print(str(num) + " -> " + str(cmd_bytes))
    ser.write(cmd_bytes)
    time.sleep(0.03)

    # cmd_bytes = bytearray(output_string3)
    # print(str(num) + " -> " + str(cmd_bytes))
    # ser.write(cmd_bytes)

    # time.sleep(0.02) # карта не успевает писать и виснет - требует переинициализации - которая ведет к частичной потере данных
    time.sleep(0.03)
    # time.sleep(0.033)
