#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import serial
import sys
import struct
from datetime import datetime

if len(sys.argv) < 2:
    print("USAGE: %s [port]" % sys.argv[0])
    sys.exit(1)

# ser = serial.Serial(sys.argv[1], sys.argv[2], stopbits=serial.STOPBITS_ONE, timeout=0.001)
ser = serial.Serial(sys.argv[1], sys.argv[2], stopbits=serial.STOPBITS_ONE)
#ser = serial.Serial(sys.argv[1], baudrate=1200)

if(ser.isOpen() == False):
    ser.open()

count = 0

while 1 :
    out = ''
    seq = []
    time.sleep(0.02)

    # modbus read
    while ser.inWaiting() > 0:
        for c in ser.read():
            seq.append(c)
            out = ''.join(("%02X " % v) for v in seq)
            if chr(c) == '\n':
                break
    if out != '':
        count += 1
        print(str(count) + " : " + str(datetime.now().timestamp()) + " : " + out)
