#include "main.h"
#include "ili9341.h"
#include "stm32f429i_discovery_ts.h"

static TS_StateTypeDef  TS_State;
static uint8_t Calibration_Done = 0;
static int16_t  A1, A2, B1, B2;

static int16_t aPhysX[2], aPhysY[2], aLogX[2], aLogY[2];

static void TouchscreenCalibration_SetHint(void);
static void GetPhysValues(int16_t LogX, int16_t LogY, int16_t * pPhysX, int16_t * pPhysY);
static void WaitForPressedState(uint8_t Pressed);

void ts_init(void)
{
    A1 = 0xFC1F;
    A2 = 0x03D5;
    B1 = 0xE92D;
    B2 = 0x04F2;

    // Horizontal orientation
    BSP_TS_Init(240, 320);
}

void Touchscreen_Calibration(void)
{
    uint8_t i = 0;
    uint8_t status = 0;

    // Horizontal orientation
    status = BSP_TS_Init(240, 320);

    TouchscreenCalibration_SetHint();

    if (status != TS_OK)
    {
        ILI9341_FillScreen(ILI9341_BLACK);
        ILI9341_WriteString(0, 240 - 105, "ERROR", Font_8x16, ILI9341_RED, ILI9341_BLACK);
        ILI9341_WriteString(0, 240 - 90, "Touchscreen cannot be initialized", Font_8x16, ILI9341_RED, ILI9341_BLACK);
    }

    while (1)
    {
        if (status == TS_OK)
        {
            aLogX[0] = 15;
            aLogY[0] = 15;
            // aLogX[1] = BSP_LCD_GetXSize() - 15;
            aLogX[1] = 305;
            // aLogY[1] = BSP_LCD_GetYSize() - 15;
            aLogY[1] = 225;

            for (i = 0; i < 2; i++)
            {
                GetPhysValues(aLogX[i], aLogY[i], &aPhysY[i], &aPhysX[i]);
            }
            A1 = (1000 * (aLogX[1] - aLogX[0])) / (aPhysX[1] - aPhysX[0]);
            B1 = (1000 * aLogX[0]) - A1 * aPhysX[0];

            A2 = (1000 * (aLogY[1] - aLogY[0])) / (aPhysY[1] - aPhysY[0]);
            B2 = (1000 * aLogY[0]) - A2 * aPhysY[0];

            Calibration_Done = 1;
            ILI9341_FillScreen(ILI9341_BLACK);
            return;
        }

        HAL_Delay(5);
    }
}

static void TouchscreenCalibration_SetHint(void)
{
  /* Clear the LCD */
  ILI9341_FillScreen(ILI9341_BLACK);

  ILI9341_WriteString(0, 240/2 - 27, "Before using the Touchscreen", Font_8x16, ILI9341_YELLOW, ILI9341_BLACK);
  ILI9341_WriteString(0, 240/2 - 12, "you need to calibrate it.", Font_8x16, ILI9341_YELLOW, ILI9341_BLACK);
  ILI9341_WriteString(0, 240/2 + 3,  "Press on the yellow dots", Font_8x16, ILI9341_YELLOW, ILI9341_BLACK);
}

/**
  * @brief  Get Physical position
  * @param  LogX : logical X position
  * @param  LogY : logical Y position
  * @param  pPhysX : Physical X position
  * @param  pPhysY : Physical Y position
  * @retval None
  */
static void GetPhysValues(int16_t LogX, int16_t LogY, int16_t * pPhysX, int16_t * pPhysY)
{
  ILI9341_FillRectangle(LogX, LogY, 5, 5, ILI9341_YELLOW);

  /* Wait until touch is pressed */
  WaitForPressedState(1);

  BSP_TS_GetState(&TS_State);
  *pPhysX = TS_State.X;
  *pPhysY = TS_State.Y;

  /* Wait until touch is released */
  WaitForPressedState(0);
  ILI9341_FillRectangle(LogX, LogY, 5, 5, ILI9341_RED);
}

static void WaitForPressedState(uint8_t Pressed)
{
  TS_StateTypeDef  State;

  do
  {
    BSP_TS_GetState(&State);
    HAL_Delay(10);
    if (State.TouchDetected == Pressed)
    {
      uint16_t TimeStart = HAL_GetTick();
      do {
        BSP_TS_GetState(&State);
        HAL_Delay(10);
        if (State.TouchDetected != Pressed)
        {
          break;
        } else if ((HAL_GetTick() - 100) > TimeStart)
        {
          return;
        }
      } while (1);
    }
  } while (1);
}

uint16_t Calibration_GetX(uint16_t x)
{
  return (((A1 * x) + B1) / 1000);
}

uint16_t Calibration_GetY(uint16_t y)
{
  return (((A2 * y) + B2) / 1000);
}

uint8_t IsCalibrationDone(void)
{
  return (Calibration_Done);
}

bool ts_get_position(int16_t* x, int16_t* y)
{
  static TS_StateTypeDef  TS_State;

  BSP_TS_GetState(&TS_State);

  /* Read the coordinate */
  if(TS_State.TouchDetected)
  {
    *x = 320 - Calibration_GetY(TS_State.Y);
    *y = - Calibration_GetX(TS_State.X);
  }
  return TS_State.TouchDetected;
}
