#ifndef __LOG_SD__
#define __LOG_SD__ 1

#include <stdbool.h>

#define LOG_DIR "/log"
#define OLD_LOG_DIR "/oldlog"
#define LOG_MAX_FILE_NUM 10000
#define LOG_FILE_MAX_LINES 10000

#define LOG_STACK_SIZE 512
#define LOG_PRIORITY (configMAX_PRIORITIES-3)

void logger_task(void);
bool logger_is_init(void);
int  log_filename(char** name);

uint32_t log_read_index(void);
void     log_give_semaphore(void);
bool 	 log_is_init(void);

uint32_t log_current_file_num(void);
uint32_t log_current_file_position(void);

#endif
