#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "ff.h"
#include "log_sd.h"
#include "uptime.h"
#include "errors.h"
#include "circular_buffer.h"
#include "sdcard.h"
#include "input.h"
#include "main.h"
#include "file_list.h"
#include "soft_timer.h"

DIR dir;
FILINFO fno;

struct 
{
    FIL file;
    uint32_t read_index;
    char     filename[64];
    uint32_t lines_count;
    bool     is_opened;
    uint32_t file_num;
    bool     is_init;
    bool     sync_flag;
    bool     write_ptr; // written bytes count
    soft_timer_t* timer; // timeout for file close 30 sec default
}logger;

#define LAST_WRITE_TIMEOUT 10000 // ms

bool log_is_init(void)
{
    return logger.is_init;
}

uint32_t log_read_index(void)
{
    return logger.read_index;
}

static int32_t get_free_file_number(void)
{
    FRESULT res;
    uint32_t max_file_num = 0;
    uint32_t min_file_num = LOG_MAX_FILE_NUM;
    TCHAR* path = LOG_DIR;
    res = f_opendir(&dir, path);
    if(res != FR_OK)
    {
        return -16;
    }
    for(;;)
    {
        res = f_readdir(&dir, &fno);
        if (res != FR_OK || fno.fname[0] == 0)
            break;
        if(!(fno.fattrib & AM_DIR))
        {
            char str[] = ".log";
            size_t slen = strlen(str);
            size_t namelen = strlen(fno.fname);
            if(namelen <= slen)
                continue;
            if(!strncmp(str, &fno.fname[namelen - slen], slen))
            {
                char* endptr;
                uint32_t curr_proj_num = strtol(fno.fname, &endptr, 10);

                if(curr_proj_num > max_file_num) // projects sorted by name
                    max_file_num = curr_proj_num;
                if(curr_proj_num < min_file_num) // projects sorted by name
                    min_file_num = curr_proj_num;
            }
        }
    }
    uint8_t project_num = 1;
    if(max_file_num > 0)
        project_num = max_file_num + 1;
    f_closedir(&dir);
    return project_num;
}

int log_filename(char** name)
{
    // if(!logger.filename[0])
    //     return -1;
    *name = logger.filename;
    return 0;
}

int gen_filename(char** name)
{
    if(!strlen(logger.filename))
    {
        // int count = 0;
        int32_t ret = get_free_file_number();
        if (ret < 0){
            return ret;
        }else{
            logger.file_num = ret;
        }
        sprintf(logger.filename, "%s/%d.log", LOG_DIR, (int)logger.file_num);
    }
    *name = logger.filename;
    return 0;
}

int log_dir(void)
{
    FRESULT res = f_stat(LOG_DIR, 0);
    if(res == FR_NO_FILE) {
        res = f_mkdir(LOG_DIR);
        if(res != FR_OK) return res;
        res = f_stat(LOG_DIR, 0);
        if(res != FR_OK) return res;
    }
    return res;
}

void log_close_file(void)
{
    f_close(&logger.file);
    logger.is_opened = false;
    logger.lines_count = 0;
    memset(logger.filename, 0, sizeof(logger.filename)); // this will help to open new file

    char filename[32];
    sprintf(filename, "%d.log", (int)logger.file_num);
    file_list_push(filename); // only name without dir
}

int write_message(uint8_t* data, uint16_t len)
{
    FRESULT res;
    UINT bw;
    if(logger.lines_count >= LOG_FILE_MAX_LINES)
    {
        if(logger.timer)
            soft_timer_stop(logger.timer);
        log_close_file();
    }

    char* filename;
    if(gen_filename(&filename) < 0)
        return -1;

    if(!logger.is_opened)
    {
        res = f_open(&logger.file, filename, FA_WRITE | FA_OPEN_APPEND);
        if(res != FR_OK) return res;
        logger.is_opened = true;
    }
    // это бинарный файл
    res = f_write(&logger.file, (uint8_t*)data, len, &bw);
    if(res != FR_OK) 
        return res;
    else
        logger.write_ptr = f_tell(&logger.file);

    if (logger.timer && soft_timer_is_active(logger.timer))
    {
        soft_timer_restart(logger.timer);
    }else{
        logger.timer = soft_timer(LAST_WRITE_TIMEOUT, log_close_file, true, false); // saving to sd
    }

    return res;
}

int open_log_file(void)
{
    FRESULT res;
    int ret = 0;
    char* filename;
    do{
        if(!sd_is_init())
        {
            if(sd_mount())
            {
                ret = -1;
                break;
            }
        }
        res = log_dir();
        if(res != FR_OK)
        {
            ret = -1;
            break;
        }
        if(gen_filename(&filename) < 0)
        {
            ret = -1;
            break;
        }
    }while(0);
    return ret;
}

void file_sync(void)
{
    if(logger.is_opened)
    {
        FRESULT res = f_sync(&logger.file);
        if(res == FR_OK)
        {
            logger.sync_flag = false;
        }
    }
}

extern circular_buffer_t input_cbuffer1;

void logger_task(void)
{
    int ret = 0;
    uint8_t message[256] = {0};

    if(!logger.is_init)
    {
        if(!open_log_file())
        {
            logger.is_init = true;
            logger.sync_flag = false;
        }
    }
    // read from input cbuffer
    do{
        int len = cbuffer_pop_message(&input_cbuffer1, message);
        if(len > 0)
        {
            ret = write_message(message, len);
            if(!ret)
            {
                ++logger.read_index;
                ++logger.lines_count;
                logger.sync_flag = true;
            }else{
                errors_on_error(SDCARD_WRITE_ERROR);
                logger.is_init = false;
                logger.is_opened = false;
                sd_reinit();
                return;
            }
        }else{
            break;
        }
    }while(1);

    if(ret < 0)
    {
        logger.read_index = 0; // drop on overflow write index
    }
    if(logger.sync_flag)
    {
        file_sync();
    }
}

char* log_current_filename(void)
{
    return logger.filename;
}

uint32_t log_current_file_position(void)
{
    return f_tell(&logger.file);
}
