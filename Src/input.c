#include <stdint.h>
#include <string.h>
#include "input.h"
#include "errors.h"
#include "log_sd.h"
#include <stdio.h>
#include "circular_buffer.h"
#include "uptime.h"
#include "list.h"
// #include "utils.h"
#ifndef UNITTEST
	#include "sadko.h"
	#include "stm32f4xx_hal.h"
#endif
#include "ff.h"

typedef struct 
{
	int channel;
	int from;
	int to;
}range_t;

typedef struct 
{
	int channel;
	int type;
}input_type_t;

list_t* input_ranges; // for filter

extern input_t input1;
extern input_t input2;
extern circular_buffer_t input_cbuffer1;

// выполняется в прерывании
void input_on_message(uint8_t* data, uint16_t len)
{
	input_t* input = &input1;
	++input->message_count;
	// push to input_cbuffer 
	uint8_t id_data[256 + 16];

	uint16_t data_length = len + INPUT_MSG_HEADER_LEN - 2; // длина в байтах плюс размер заголовка минус размер поля размера
	uint16_t mess_id = input->message_count % 0xFFFF;
	uint16_t mess_type = 1;

	id_data[0] = data_length >> 8;
	id_data[1] = data_length & 0xFF;
	id_data[2] = mess_id >> 8;
	id_data[3] = mess_id & 0xFF;
	id_data[4] = mess_type >> 8;
	id_data[5] = mess_type & 0xFF;
	memcpy(&id_data[INPUT_MSG_HEADER_LEN], data, len);

#ifdef UNITTEST
	printf("l = 0x%x len = 0x%x\n", l, len);
	memdump(id_data, len + l, 16);
#endif
#ifndef UNITTEST
	int ret = cbuffer_push_message(&input_cbuffer1, id_data, len + INPUT_MSG_HEADER_LEN);
	if(ret < 0)
		errors_on_error(CBUFFER_PUSH_FAIL);
#endif
}

// check changes in all ranges
int input_filter_data(input_t* input, uint8_t* data, int len)
{
    if(len <= 0)
        return 0;
    uint16_t i = 0;

    if(!input_ranges || !list_size(input_ranges)){
    	return 1;
    }

    item_t* item;
	list_first(input_ranges, &item);
	do
    {
    	range_t *r = list_data(input_ranges, &item);
    	if(r)
    	{
    		if(r->from > len)
    		{
    			return 1; // если сообщение короче диапазона фильтра - то оно проходит
    		}
		    for(i = r->from; (i < len) && (i <= r->to); ++i)
		    {
		        if(data[i] != input->buffer[i])
		            return 1;
		    }
    	}
    }while(list_next(input_ranges, &item));
    return 0; // no changes
}

#ifndef UNITTEST
// для LCD
void input_data(char* string)
{
	input_t* input = &input1;
	char* filename;
	log_filename(&filename);
	char name[] = "             ";
	strcpy(name, filename);
	sprintf(string, "message received:%ld\n"
					"message saved:   %ld\n"
					"log file: %s\n"
					"write position: %4ld\n"
					"in buffer count: %4d",
	    				 input->message_count,
	    				 log_read_index(),
	    				 filename,
						 log_current_file_position(),
	    				 input_cbuffer1.count
				 );
}
#endif

void find_ranges(FIL* file)
{
	char line[255];
	do{
		char* msg = f_gets((TCHAR*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[ranges]";
		char* ranges = strstr(msg, pattern);
		if(ranges)
		{
			do{
				msg = f_gets((TCHAR*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				char* range = strstr(msg, "(");
				if(!range)
					continue;
				int channel = atoi((char*)&range[1]);
				char* echan = strstr(msg, ":");
				if(!echan)
					continue;
				int from = atoi((char*)&echan[1]);

				char* _to = strstr(msg, ",");
				if(!_to)
					continue;
				int to = atoi((char*)&_to[1]);

				range_t r = {.channel = channel, .from = from, .to = to};

				list_insert_tail(input_ranges, &r);
			}while(1);
		}
	}while(1);
}

void find_type(FIL* file)
{
	char line[255];
	do{
		char* msg = f_gets((TCHAR*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[input_type]";
		char* types = strstr(msg, pattern);
		if(types)
		{
			do{
				msg = f_gets((TCHAR*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				char* type_str = strstr(msg, "(");
				if(!type_str)
					continue;
				int channel = atoi((char*)&type_str[1]);

				char* _type = strstr(msg, ":");
				if(!_type)
					continue;
				int ch_type = atoi((char*)&_type[1]);

				input_t* in = channel == 1 ? &input1 : &input2;
				in->type = ch_type;

			}while(1);
		}
	}while(1);
}

void inputs_configure(void)
{
	FIL file;
    FRESULT res = f_open(&file, ".input_config", FA_READ);
	if(res != FR_OK) return;
	find_ranges(&file);
	find_type(&file);
	f_close(&file);
}

void input_init(input_t* input)
{
}
