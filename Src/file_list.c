#include "file_list.h"
#include <string.h>
#include <stdio.h>
#include <string.h>

file_list_t fl;

int file_list_push(char* filename) // записать имя файла
{
    // записать новую строку в конец файла
    FRESULT res = f_open(&fl.file, ".file_list", FA_READ | FA_WRITE | FA_OPEN_APPEND);
    if(res != FR_OK) 
        return -1;
    int ret = f_printf(&fl.file, "%s\n", filename);
    f_close(&fl.file);
    if(ret <= 0)
        return ret;
    return 0;
}

int file_list_peek(char* filename, int maxlen) // взять имя файла, не удаляя из списка
{
    int ret = 0;
    FILINFO fno;
    FRESULT res = f_stat(".file_list", &fno);
    if(res != FR_OK)
        return -1;

    // считать первую строку
    res = f_open(&fl.file, ".file_list", FA_READ);
    if(res != FR_OK)
        return -1;

    TCHAR* buff = f_gets((TCHAR*)filename, maxlen, &fl.file);
    if(buff != NULL)
    {    
        int eof = strlen(filename); // exclude '\n'
        if(eof > 1)
            filename[eof - 1] = 0;
    }else{
        ret = -2;
    }
    f_close(&fl.file);
    return ret;
}

int file_list_pop(void) // удалить верхнее имя файла из списка
{
    #define MAX 256
    FIL fptr1 = {0}, fptr2 = {0};
    char fname[] = ".file_list";
    char str[MAX], temp[] = ".temp";

    FRESULT res = f_open(&fptr1, fname, FA_READ);
    if(res != FR_OK)
    {
        return -1;
    }
    res = f_open(&fptr2, temp, FA_READ | FA_WRITE | FA_OPEN_ALWAYS);
    if(res != FR_OK)
    {
        f_close(&fptr1);
        return -2;
    }
    // copy all contents to the temporary file except first line
    f_gets(str, MAX, &fptr1);
    while(!f_eof(&fptr1))
    {
        f_gets(str, MAX, &fptr1);
        f_printf(&fptr2, "%s", str);
    }
    f_close(&fptr1);
    f_close(&fptr2);
    f_unlink(fname);          // remove the original file 
    f_rename(temp, fname);    // rename the temporary file to original name    
    return 0;
}

int file_list_init(void)
{
    // nothing
    return 0;
}
