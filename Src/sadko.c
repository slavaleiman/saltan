#include "sadko.h"
#include "input.h"
#ifndef UNITTEST
#include "usart_port.h"
#include "modbus.h"
#include "errors.h"
#include "CRC.h"
#include "circular_buffer.h"
#include "sdcard.h"
#include "ff.h"
#include "log_sd.h"
#include "file_list.h"
#include <stdio.h>
#include <stdlib.h>

extern usart_port_t uport7; // output

#endif

#define SADKO_MAX_MSG_LEN (125 * 2) // почему 125 см. doc/mb_protocol.doc (протокол садко)

// здесь лежат сообщения на выходе 
// буфер должен обновляться после кадого подтверждения отправки

// CIRCULAR_BUFFER_DEF(sadko_cbuffer, SADKO_CBUFFER_SIZE)

sadko_t sadko;

#ifndef UNITTEST
void sadko_on_message(uint8_t* data, uint16_t len)
{
    int ret = modbus_on_rtu(sadko.uport, data, len);
    if(!ret)
    {
        uport_transmit(sadko.uport);
    }else{
        errors_on_error(ret);
        return;
    }
}

void sadko_set_modbus_addr(uint8_t addr)
{
    sadko.uport->addr = addr;
}
#endif

void sadko_task(void)
{
    if(!log_is_init())
        return;
    if(!sd_is_init())
    {
        if(sd_mount())
        {
            return;
        }
    }
    sadko_update();
}

void save_output_position(void)
{
    uint8_t data[68];
    sadko_settings_t set;
    set.read_ptr = sadko.approved_ptr;
    memcpy(data, &set, sizeof(sadko_settings_t));
    sprintf((char*)&data[4], "%s", sadko.filename);
    sd_write_data(".sadko", (uint8_t*)data, sizeof(sadko_settings_t), 0);
}

// num - порядковый номер
uint16_t get_msg_length(uint8_t* msg)
{
    uint16_t len = ((msg[0] << 8) | msg[1]) + 2; // длина всего сообщения + поле длины (байт)
    return len;
}

int shift_output_buffer(uint16_t msg_count)
{
    uint8_t skip = 0; // read msg position
    if(sadko.output_length == 0)
        return 0;
    uint8_t m = 1;
    for(; m <= sadko.messages_in_outbuffer; ++m)
    {
        uint16_t len = get_msg_length(&sadko.output_buffer[skip]);
        skip += len;
        if(msg_count == m)
        {
            break;
        }
    }
    int16_t movelen = sadko.output_length - skip;
    if(movelen > 0)
    {
        memmove(sadko.output_buffer, &sadko.output_buffer[skip], movelen);
        sadko.output_length = movelen;
    }else{
        sadko.output_length = 0;
    }
    if(sadko.messages_in_outbuffer >= m){
        sadko.messages_in_outbuffer -= m;
    }else{
        sadko.messages_in_outbuffer = 0;
    }
    return skip; // bytes
}

int sadko_drop_file(void) // дропаем старый файл
{
    // TODO check filename
    f_close(&sadko.file);
    sadko.is_file_opened = false;
    int ret = file_list_pop();
    sadko.approved_ptr = 0;
    sadko.file_read_ptr = 0;
    sadko.output_length = 0;
    
    memset(sadko.filename, 0, sizeof(sadko.filename));
    memset(&sadko.fno, 0, sizeof(FILINFO));

    return ret;
}

int check_can_drop_file(void)
{
    if(sadko.fno.fsize == 0)
    {
        return 1;
    }
    if(!sadko.is_file_opened) // нечего дропать
    {
        return 0;
    }
    if(sadko.approved_ptr == sadko.fno.fsize) 
    {
        return 1;
    }
    return 0;
}

void update_input_regs(void)
{
    // наверное это не правильно хранить значение в формате для отправки
    sadko.input_regs1[0] = __REV16(0x6);
    sadko.input_regs1[1] = __REV16(0x6);
    sadko.input_regs1[2] = __REV16(sadko.output_length);
    sadko.input_regs1[3] = __REV16((sadko.output_length + 1) / 2);
    sadko.input_regs1[4] = 0;
    sadko.input_regs1[5] = __REV16(sadko.messages_in_outbuffer);
    sadko.input_regs1[6] = __REV16(1);
    sadko.input_regs1[7] = 0;
    sadko.input_regs1[8] = 0;
}

void sadko_process_approve(void)
{
    int skip = shift_output_buffer(sadko.approved_count);
    if(skip > 0)
    {
        sadko.approved_ptr += skip;
        save_output_position();
        update_input_regs();
    }
    sadko.approved_count = 0;
}

void sadko_recieve_approve(uint16_t from_id, uint16_t to_id)
{
    // если пришли не правильные ID - ничего не двигаем
    uint16_t count = to_id - from_id + 1;
    sadko.approved_count = count;
    sadko.output_ready = false;
}

int sadko_write_data(uint16_t* buffer, uint16_t from_addr, uint8_t num_regs)
{
    if((from_addr & 0xF000) == 0x1000)
    {
        memcpy(sadko.holding_regs1, buffer, num_regs * 2);
        // sadko.command call update read_index here !!!
        if(num_regs > 1)
        {
            uint16_t cmd = __REVSH(buffer[0]);
            uint16_t param = __REVSH(buffer[1]);
            if((cmd == CMD_APPROVE) && (param == 1))
            {
                uint16_t first_id = __REVSH(buffer[2]);
                uint16_t last_id = __REVSH(buffer[3]);
                sadko_recieve_approve(first_id, last_id);
            }
        }
        // при подтверждении отправки
    }
    else if((from_addr & 0xF000) == 0x2000)
    {   
        // 0x2000 // uint16_t  Широта – градусы
        // 0x2001 // uint16_t  Широта – минуты
        // 0x2002 // uint16_t  Широта - десятитысячные доли минут
        // 0x2003 // uint16_t  Долгота – градусы
        // 0x2004 // uint16_t  Долгота – минуты
        // 0x2005 // uint16_t  Долгота - десятитысячные доли минут
        // 0x2006 // uint16_t  Код символа “E” для восточной или “W” для западной долготы
        // 0x2007 // uint16_t  Код символа “N” для северной или “S” для южной широты

        // 0x2008 int64_t      Текущее время GPS (формат UnixTime UTC), байты 1-2 (младшие)
        // 0x2009 int64_t      Текущее время GPS (формат UnixTime UTC), байты 3-4
        // 0x200A int64_t      Текущее время GPS (формат UnixTime UTC), байты 5-6
        // 0x200B int64_t      Текущее время GPS (формат UnixTime UTC), байты 7-8

        memcpy(sadko.holding_regs2, buffer, num_regs * 2);
    }
    else if((from_addr & 0xF000) == 0x8000)
    {   
        memcpy(sadko.holding_regs8, buffer, num_regs * 2);
    }

    return 0;
}

// int sadko_get_message(void)
// {
    // uint16_t offset = from_addr & 0xFFF;
    // if(offset > sadko.input_regs[MSG_AVAILABLE_REG])
        // return -1;

    // 0x2000 uint16_t Длина записи 1 (в байтах, включая номер записи) = n
    // 0x2001 uint16_t Запись данных 1 – номер записи = sadko.msg_id & 0xFFFF;
    // 0x2002 uint16_t Запись данных 1 – данные 1
    // 0x2003 uint16_t Запись данных 1 – данные 2
    // n uint16_t      Запись данных 1 – окончание данных
    // n+1 uint16_t    Длина записи 2
    // n+2 uint16_t    Запись данных 2 – номер записи
    // n+3 uint16_t    Запись данных 2 – данные 1
    // n+4 uint16_t    Запись данных 2 – данные 2

    // copy from buffer sadko.message; 
    // memcpy(buffer, &sadko.message[offset], num_regs * 2); // /*2 = sizeof(uint16_t*/
    // return 0;
// }

int sadko_read_input(uint16_t *buffer, uint16_t from_addr, uint8_t num_regs)
{
    // data address
    if((from_addr & 0xF000) == 0x2000)
    {
        memcpy(buffer, sadko.output_buffer, num_regs * 2);
    }
    else if((from_addr & 0xF000) == 0x1000)
    {
        if(num_regs < 10)
        {
            uint16_t offset = from_addr & 0xFFF;
            memcpy(buffer, &sadko.input_regs1[offset], num_regs * 2); // /* 2 = sizeof(uint16_t) */
        }
    }
    return 0;
}

int file_peek(void)
{
    char filename[50];
    int ret = file_list_peek(filename, 50);
    if(!ret)
    {
        // если считанное имя не совпадает со считанным из конфига
        // дропаем позицию.
        // 5 - потому что path = "/log/"
        if(strcmp(&sadko.filename[strlen(LOG_DIR)+1], filename))
        { // если имя обновилось
            sadko.file_read_ptr = 0;
            sadko.approved_ptr = 0;
            sprintf(sadko.filename, "%s/%s", LOG_DIR, filename);
        }
    }else{
        // если файл лист пустой
        sadko.filename[0] = 0;
        sadko.approved_ptr = 0;
        sadko.file_read_ptr = 0;
    }
    return ret;
}

#ifndef UNITTEST
int sadko_read_config(void)
{
    uint8_t data[sizeof(sadko_settings_t)] = {0};
    int ret = sd_read_data(".sadko", (uint8_t*)&data, sizeof(sadko_settings_t), 0);
    if(!ret)
    {
        sadko_settings_t* set = (sadko_settings_t*)data;
        sadko.file_read_ptr = set->read_ptr;
        sadko.approved_ptr = set->read_ptr;
        sprintf(sadko.filename, "%s", set->filename);
    }else{
        sadko.file_read_ptr = 0;
    }
    return ret;
}
#endif

#ifndef UNITTEST
int check_file_exist_and_non_empty(char* path)
{
    FILINFO *fno = &sadko.fno;
    FRESULT res = f_stat(path, fno);
    if(res == FR_NO_FILE)
    {
        return 1;
    }
    if(res == FR_DISK_ERR)
    {
        return -1; // leads to reinit sd
    }
    if(!fno->fsize)
    {
        return 1; // file empty
    }
    if(fno->fsize == sadko.file_read_ptr)
    {
        return 1;
    }
    return 0;
}

int open_file(FIL* file)
{
    do
    {
        int ret = file_peek();
        if(ret < 0)
            return -1;
        // алгоритм исключает появление пустых файлов, но может файл был удален руками людей
        ret = check_file_exist_and_non_empty(sadko.filename);
        if(ret > 0) // no file
        {
            file_list_pop();
            memset(sadko.filename, 0, sizeof(sadko.filename));
            memset(&sadko.fno, 0, sizeof(FILINFO));
        }
        else if(ret < 0)
        {
            return ret; // это что то совсем плохое
        }
        else{
            break;
        }
    }while(1);

    FRESULT res = f_open(file, sadko.filename, FA_READ);
    if(res != FR_OK) 
        return -1;

    return 0;
}
#endif

// тут нужно перепаковать сообщение в регистры в зависимости от формата
void sadko_process_output_format(uint8_t* msg_full, uint16_t *len)  // длина без первых двух байт
{
    // заголовок уже лежит в сообщении, нужно правильно разложить данные
    uint8_t* msg = &msg_full[INPUT_MSG_HEADER_LEN]; // len + id + msg_type

    uint16_t data_length = *len - INPUT_MSG_HEADER_LEN; // длина данных

    // заполняем данными после заголовка
    uint8_t result[256];
    uint8_t result_len = 0;

    if(msg[0] == TYPE_KAHOVKA)
    {
        result_len = 12;
        // для каховской сварки формат определяется длиной сообщения
        if(data_length == 7)
        {
            result[0] = 0;
            result[1] = msg[2];
            
            result[2] = msg[3];
            result[3] = msg[4];

            result[4] = 0;
            result[5] = msg[5];
            
            result[6] = 0;
            result[7] = msg[6];
            
            result[8] = 0;
            result[9] = msg[7];

            result[10] = 0;
            result[11] = msg[8];
        }
        else if(data_length == 11)
        {
            result[0] = 0;
            result[1] = msg[2];
            
            result[2] = msg[3];
            result[3] = msg[4];

            result[4] = msg[5];
            result[5] = msg[6];
            
            result[6] = msg[7];
            result[7] = msg[8];
            
            result[8] = msg[9];
            result[9] = msg[10];
            
            result[10] = msg[11];
            result[11] = msg[12];
        }
        memcpy(&msg_full[INPUT_MSG_HEADER_LEN], result, result_len);
        *len = result_len + INPUT_MSG_HEADER_LEN;
    }
}

int sadko_file_prepare(void)
{
    if(!sadko.is_file_opened)
    {
        int ret = open_file(&sadko.file);
        if(!ret)
            sadko.is_file_opened = true;
        else{
            return -1;
        }
    }
    if(sadko.approved_count > 0)
    {
        sadko_process_approve();
    }
    // если дочитали до конца файла - ждем подтверждения отправки
    if(check_can_drop_file())
    {
        if(!sadko.output_length) // если не осталось данных в буфере
        {
            sadko_drop_file();
        }
        return -2;
    }
    int ret = 0;
    if(!sadko.message_length)
        ret = sd_read_message(&sadko.file, sadko.file_read_ptr, sadko.message, &sadko.message_length);
    if(ret == -4)
    {   // прочитано 0 байт конец файла
        return ret;
    }
    if(ret < 0) // bad file - switch to next file
    {
        sadko_drop_file(); // если файл был некорректно записан - он дропается
        return ret;
    }
    return ret; // file ready to read next data
}

// id который не был отправлен
// если он был считан, но не переложен в output_buffer, 
// то считывать его не нужно
// тоесть перед чтением проверять id считанного сообщения !!!
// если сообщение уже было считано в прошлый раз, 
// то пропускаем этап считывания, пробуем его положить в output_buffer

int fill_output_buffer(void)
{
    int ret = 0;
    int16_t free_bytes = 0;
    do{
#ifndef UNITTEST // этот кусок можно вынести в отдельную функцию
        if(sadko_file_prepare() < 0)
            break;
#endif
        sadko_process_output_format(sadko.message, &sadko.message_length);
        free_bytes = SADKO_MAX_MSG_LEN - (sadko.output_length + sadko.message_length);
        if(!ret)
        {
            if(free_bytes >= 0)
            {
                // пока есть место в output_buffer, кладем туда сообщения читая их из карты
                memcpy(&sadko.output_buffer[sadko.output_length], sadko.message, sadko.message_length);
                ++sadko.messages_in_outbuffer;
                sadko.file_read_ptr = f_tell(&sadko.file);
                sadko.output_length += sadko.message_length; // + поле длины записи
                update_input_regs();
                // сообщение успешно переложено в outbuffer 
                // можно загружать следующее 
                sadko.message_length = 0;
            }else{
                break;
            }
        }else{
            f_close(&sadko.file);
            sadko.is_file_opened = false;
            break;
        }
    }while(free_bytes > 0);

    if(sadko.output_length > 0)
    {
        sadko.output_ready = true; // флаг
    }
    return 0;
}

int sadko_update(void)
{
    int ret = 0;
    if(sadko.output_ready) // буфер заполнен можно отдавать на запрос input_reg output_buffer
    {
        return 0;
    }
    do{
        if(!sadko.is_config_read)
        {
            // читаем имя файла и позицию которые могли устареть
            // если их нет с file_list - они устарели и надо обнулять
            ret = sadko_read_config(); 
            if(ret)
            {
                break;
            }
            sadko.is_config_read = true;
        }
        ret = fill_output_buffer();
        if(ret)
        {
            break;
        }
    }while(0);
    
    return ret;
}

#ifndef UNITTEST
void sadko_data(char* string)
{
    sprintf(string, "file: %s\n"
                    "size: %8ld\n"
                    "readptr: %8ld\n"
                    "readptr: %8ld\n"
                    "outbuffer: %8d\n"
                    "output_length:%8d\n"
                    "is_file_opened: %d",
                     sadko.filename,
                     (uint32_t)sadko.fno.fsize,
                     (uint32_t)sadko.file_read_ptr,
                     (uint32_t)sadko.approved_ptr,
                     sadko.messages_in_outbuffer,
                     sadko.output_length,
                     sadko.is_file_opened
                     );
}

int sadko_init(usart_port_t* port, uint8_t addr)
{
    // port read write callbacks
    sadko.uport = port;
    sadko.uport->addr = addr;
    sadko.uport->read_data = sadko_read_input;
    sadko.uport->write_data = sadko_write_data;

    sadko.output_ready = false;  // если true -> можно отправлять сообщение в садок
    sadko.is_file_opened = false;
    sadko.is_config_read = false;

    sadko.approved_ptr = 0;
    sadko.approved_count = 0;
    return 0;
}
#endif
