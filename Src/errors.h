#ifndef _ERROR_H_
#define _ERROR_H_ 1

// #ifndef UNITTEST
// #include "stm32f4xx.h"
// #include "main.h"
// #endif
#include <stdbool.h>
#include <stdint.h>

#define MAX_ERRORS 36

#define ERROR(err) \
errors_on_error(err);
	
typedef enum
{
    NO_ERROR = 0,
    CONFIG_LOAD_ERROR,
    RS232_OVERRUN_ERROR,
    RS485_OVERRUN_ERROR,
    MODBUS_HOLDING_REG_LOCKED,
	MODBUS_PORT_CANNOT_WRITE,
    MODBUS_PORT_CANNOT_READ,
    MODBUS_PORT_READ_ERROR,
	MODBUS_TX_ERROR,
    SDCARD_READ_ERROR,
    SDCARD_WRITE_ERROR,
    CBUFFER_PUSH_FAIL,    
    ERRORS_SIZE
}error_t;

const char* str_error(int32_t err);
void     errors_on_error(int err);
void     errors_init(void);
uint8_t* errors_buffer(void);
uint16_t errors_size(void); // bytes
void 	 errors_log_errors(void);
bool 	 errors_update(void);
bool 	 errors_is_update(void);
void 	 errors_update_clear(void);
bool     errors_is_error(int err); // for test
#endif //_ERROR_H_
