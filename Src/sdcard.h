
#ifndef __SDCARD_H_
#define __SDCARD_H_

#include "ff.h"
#include <stdbool.h>

int sd_write_data(char* filename, uint8_t* data, uint16_t len, uint8_t flags);
int sd_read_data(char* filename, uint8_t* data, uint16_t len, uint32_t from);
int sd_read_message(FIL* file, uint32_t read_ptr, uint8_t* data, uint16_t* len);

int  sd_mount(void);
void sd_init(void);
bool sd_is_init(void);
void sd_deinit(void);


#endif /* __SDCARD_H_ */
