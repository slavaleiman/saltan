#ifndef _INPUT_H_
#define _INPUT_H_ 1

#include "uptime.h"
#include "list.h"

typedef struct input_s input_t;

struct input_s
{
	uint8_t id; // нужно для определения типа в конфиге
	uint8_t  buffer[255];
	
	uint32_t message_count;
	uint32_t saved;

	uint16_t type; // может быть два входа с разным типом данных - тип прописывается в конфиге 
	// [input_type]
	// (1:1) - вход 1 - usart1 : TYPE_KAHOVKA
	// (2:9) - вход 2 - usart3 : TYPE_UIN
}; // welding head data

typedef struct __packed{
	uint16_t len;
	uint16_t id;
	uint16_t type;
}input_msg_header_t;

// отличия режимов 
typedef enum{
	TYPE_KAHOVKA = 1,
	TYPE_UIN = 9,
	TYPE_DIGNOSTIC = 11,
	// WHAT ELSE
}record_type;

#define INPUT_MSG_HEADER_LEN 6 // sizeof(input_msg_header_t)

void input_on_message(uint8_t* data, uint16_t len);
void input_data(char* string);
int  input_filter_data(input_t* input, uint8_t* data, int len); // проверка на изменение важных значений
void input_init(input_t* input);
void inputs_configure(void);

#endif
