#include <unistd.h>
#include "unity.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "sadko.h"

void setUp(void)
{
}

void tearDown(void)
{

}

bool log_is_init(void)
{
    return true;
}

bool sd_is_init(void)
{
    return true;
}

int sd_mount(void)
{

}

int sd_write_data(char* filename, uint8_t* data, uint16_t len, uint8_t flags)
{

}

int16_t __REVSH(int16_t value)
{
    return value; // здесь другой эндианес и не надо разворачивать !!!!! ПРОВЕРЬ ЭТО!!!!!
}

int32_t __REV16(uint32_t value)
{
    return value;
}

extern sadko_t sadko;

// int sadko_init(usart_port_t* port, uint8_t addr)
int sadko_init(uint8_t addr)
{
    // port read write callbacks
    // sadko.uport = port;
    // sadko.uport->addr = addr;
    // sadko.uport->read_data = sadko_read_input;
    // sadko.uport->write_data = sadko_write_data;
    sadko.output_id = 0;

    sadko.output_ready = false;  // если true -> можно отправлять сообщение в садок
    sadko.is_file_opened = false;

    return 0;
}

int sadko_read_config(void)
{
    int fd;
    // lock mutex here? no
    uint8_t data[8];
    // int ret = sd_read_data(".sadko", (uint8_t*)&data, 8, 0);
    // if(!ret)
    // {
        sadko.file_num      = 0; // data[0] | (data[1] << 8);
        sadko.file_read_ptr = 0; // data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24);
        sadko.output_id     = 0; // data[6] | (data[7] << 8);
    // }
    char filename[64];

    sprintf(filename, "%s/input_data_%04x.log", LOG_DIR, sadko.file_num);
    // res = f_stat(filename, 0);
    // if(res == FR_OK)
    //     return 0;
    // if(res == FR_NO_FILE)
    // {
    //     res = f_opendir(&dir, LOG_DIR);
    //     if(res != FR_OK)
    //     {
    //         return -16;
    //     }
    //     for(;;)
    //     {
    //         res = f_readdir(&dir, fno);
    //         if (res != FR_OK || fno->fname[0] == 0)
    //             break;
    //         if(!(fno->fattrib & AM_DIR))
    //         {
    //             char str[] = "input_data_";
    //             size_t len = strlen(str);
    //             if(!strncmp(str, fno->fname, len))
    //             {
    //                 char* endptr;
    //                 uint32_t curr_file_num = strtol(&fno->fname[len + 1], &endptr, 10);

    //                 sadko.file_num = curr_file_num;
    //                 sadko.output_id = 0;
    //                 save_output_position();
    //                 return 0;
    //             }
    //         }
    //     }
    // }
    return -1;
}

void test_read_msg(void)
{
    char filename[] = "../toSD/.input_config";
    FILE* fd = fopen(filename, "r");
    if(!fd) 
        return 1;


    // find_ranges(fd, filename);
    // rewind(fd);
    // find_types(fd, filename);

}

int test_endiannes(void)
{
  uint8_t s[8] = {0};
  uint16_t va = 0xAA00;
  memcpy(s, &va, 2);  // 0x00 0xAA 0x00 0x00 - little endian - lsb first
  if(s[0] == 0)
    return 1;
  else
    return 0xB;
}

int main(void)
{
    UNITY_BEGIN();

    test_endiannes();

    // sadko читает сообщения из карты которую нужно сэмулировать файлом -> туду заглушка
    RUN_TEST(test_read_msg);

    // управляется мастером по модбасу, нужно проверить что приходящие modbus сообщения управляют отдачей данных
    // RUN_TEST(test_empty_input);
    // возможно будут различия в endiannes - проверь как работает memcpy в тесте и на железе

    return UNITY_END();
}
